package lab.aikibo.switchbpn.controller

import lab.aikibo.switchbpn.model.ResponseData
import lab.aikibo.switchbpn.services.SpptServices
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux

@RestController
class ApiController {

    @Autowired
    lateinit var spptServices: SpptServices

    @RequestMapping("/getAllData")
    fun getAllData(): Flux<ResponseData> = spptServices.getAll()

    @RequestMapping("/getData/{nop}")
    fun getByNop(@PathVariable("nop") nop: String) = spptServices.getByNop(nop)

}