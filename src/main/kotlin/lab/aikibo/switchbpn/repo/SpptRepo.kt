package lab.aikibo.switchbpn.repo

import lab.aikibo.switchbpn.entity.Sppt
import lab.aikibo.switchbpn.entity.id.SpptPK
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface SpptRepo: JpaRepository<Sppt, SpptPK> {

    fun findByThnPajakSppt(thnPajakSppt: String): List<Sppt>

}