package lab.aikibo.switchbpn.entity.id

import java.io.Serializable

data class SpptPK(
        var kdPropinsi: String,
        var kdDati2: String,
        var kdKecamatan: String,
        var kdKelurahan: String,
        var kdBlok: String,
        var noUrut: String,
        var kdJnsOp: String,
        var thnPajakSppt: String
): Serializable {
    constructor(): this("", "", "", "", "", "", "", "")
}